#pragma once
#include "bmp_header.h"
#include "image.h"
#include <stdio.h>
#include <stdlib.h>


enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    MEMORY_ERROR
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum read_status from_bmp(FILE *in, struct image *image);

enum write_status to_bmp(FILE *out, struct image const *image);
