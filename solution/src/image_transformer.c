#include "image_transformer.h"
#include <stdio.h>

#define PIXEL_SIZE 3

static uint32_t address_from_coordinates(uint32_t row, uint32_t column, uint32_t width) {
    return row * width + column;
}

struct image rotate( struct image const source ) {
    struct image rotated_image = {0};
    int32_t width = (int32_t) source.width;
    int32_t height = (int32_t) source.height;
    rotated_image.data = (struct pixel *) malloc(width * height * PIXEL_SIZE);
    if (rotated_image.data == NULL) {
        fprintf(stderr, "Out of memory \n");
        abort();
    }
    rotated_image.height = width;
    rotated_image.width = height;
    for (int32_t column = 0; column < width; column++) {
        for (int32_t row = height - 1; row >= 0; row--) {
            rotated_image.data[column * height + (height - 1 - row)] = source.data[address_from_coordinates(row, column, width)];
        }
    }
    return rotated_image;
}
