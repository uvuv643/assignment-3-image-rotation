#include "bmp_transformer.h"

#define BMP_SIGNATURE 0x4d42
#define BMP_PIXEL_DATA_OFFSET 54
#define BMP_HEADER_SIZE 40
#define BMP_PLANES 1
#define BMP_BITS_PER_PIXEL 24
#define BMP_COMPRESSION 0
#define PIXEL_SIZE 3
#define BMP_PADDING_BIT_COUNT 4

static uint32_t calculate_padding(uint32_t width) {
    return (BMP_PADDING_BIT_COUNT - (width * PIXEL_SIZE % BMP_PADDING_BIT_COUNT)) % 4;
}

static uint32_t calculate_image_size(uint32_t width, uint32_t height) {
    return (PIXEL_SIZE * width + calculate_padding(width)) * height;
}

static int8_t bmp_header_check(struct bmp_header* bmp_header) {
    if (calculate_image_size(bmp_header->biWidth, bmp_header->biHeight) > bmp_header->biSizeImage) return 0;
    if (bmp_header->bfileSize - bmp_header->biSizeImage - BMP_HEADER_SIZE < 0) return 0;
    if (bmp_header->bfType != BMP_SIGNATURE) return 0;
    if (bmp_header->biPlanes != BMP_PLANES) return 0;
    if (bmp_header->biBitCount != BMP_BITS_PER_PIXEL) return 0;
    if (bmp_header->biCompression != BMP_COMPRESSION) return 0;
    if (bmp_header->biSize != BMP_HEADER_SIZE) return 0;
    if (bmp_header->bOffBits != BMP_PIXEL_DATA_OFFSET) return 0;
    if (bmp_header->bfReserved != 0) return 0;
    return 1;
}

static struct bmp_header headers_from_image(struct image const *image) {
    uint32_t bmp_content_size = calculate_image_size(image->width, image->height);
    return (struct bmp_header) {
        .bfType = BMP_SIGNATURE,
        .bfileSize = BMP_PIXEL_DATA_OFFSET + bmp_content_size,
        .bfReserved = 0,
        .bOffBits = BMP_PIXEL_DATA_OFFSET,
        .biSize = BMP_HEADER_SIZE,
        .biWidth = image->width,
        .biHeight = image->height,
        .biPlanes = BMP_PLANES,
        .biBitCount = BMP_BITS_PER_PIXEL,
        .biCompression = BMP_COMPRESSION,
        .biSizeImage = bmp_content_size,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
    };
}

static uint32_t address_from_coordinates(uint32_t width, uint32_t padding, uint32_t pixel_row, uint32_t pixel_column) {
    return ((width * PIXEL_SIZE + padding) * (pixel_row) + pixel_column * PIXEL_SIZE);
}

enum read_status from_bmp(FILE *in, struct image* image) {
    struct bmp_header bmp_headers = {0};
    fseek(in, 0, 0);
    if (fread(&bmp_headers, BMP_HEADER_SIZE, 1, in) != 1) {
        return READ_INVALID_SIGNATURE;
    }
    if (!bmp_header_check(&bmp_headers)) {
        return READ_INVALID_HEADER;
    }
    uint32_t height = bmp_headers.biHeight;
    uint32_t width = bmp_headers.biWidth;
    uint32_t image_size = bmp_headers.biSizeImage;
    uint32_t padding = calculate_padding(width);

    image->width = width;
    image->height = height;
    image->data = (struct pixel *) malloc(width * height * PIXEL_SIZE);
    uint8_t *bmp_data = (uint8_t *) malloc(image_size);
    if (image->data == NULL || bmp_data == NULL) {
        fprintf(stderr, "Out of memory \n");
        abort();
    }
    fseek(in, BMP_PIXEL_DATA_OFFSET, 0);
    if (fread(bmp_data, image_size, 1, in) != 1) {
        free(bmp_data);
        free(image->data);
        return READ_INVALID_BITS;
    }
    for (uint32_t row = 0; row < height; row++) {
        for (uint32_t column = 0; column < width; column++) {
            uint32_t initial_address = row * width + column;
            uint32_t target_address = address_from_coordinates(width, padding, row, column);
            image->data[initial_address] = (struct pixel) {
                bmp_data[target_address], bmp_data[target_address + 1], bmp_data[target_address + 2]
            };
        }
    }
    free(bmp_data);
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const* image) {
    struct bmp_header bmp_headers = headers_from_image(image);
    if (fwrite(&bmp_headers, BMP_HEADER_SIZE, 1, out) != 1) {
        return WRITE_ERROR;
    }
    uint32_t width = image->width;
    uint32_t height = image->height;
    uint32_t image_size = bmp_headers.biSizeImage;
    uint32_t padding = calculate_padding(width);
    uint8_t *bmp_data = (uint8_t *) malloc(image_size);
    if (bmp_data == NULL) {
        fprintf(stderr, "Out of memory \n");
        abort();
    }
    for (uint32_t bit = 0; bit < image_size; bit++) {
        bmp_data[bit] = 0;
    }
    for (uint32_t row = 0; row < height; row++) {
        for (uint32_t column = 0; column < width; column++) {
            uint32_t initial_address = address_from_coordinates(width, padding, row, column);
            uint32_t target_address = row * width + column;
            bmp_data[initial_address + 0] = image->data[target_address].b;
            bmp_data[initial_address + 1] = image->data[target_address].g;
            bmp_data[initial_address + 2] = image->data[target_address].r;
        }
    }
    fseek(out, BMP_PIXEL_DATA_OFFSET, 0);
    if (fwrite(bmp_data, 1, image_size, out) != image_size) {
        free(bmp_data);
        return WRITE_ERROR;
    }
    free(bmp_data);
    return WRITE_OK;
}
