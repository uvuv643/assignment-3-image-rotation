#pragma once
#include <stdio.h>
#include <stdlib.h>

FILE* input_file(const char* filename, const char* mode);
void close_file(FILE* file);
