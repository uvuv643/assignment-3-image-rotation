#include "bmp_transformer.h"
#include "fileIO.h"
#include "image_transformer.h"

int main(int argc, char **argv) {
    (void) argc;
    (void) argv; // suppress 'unused parameters' warning

    if (argc != 3) {
        fprintf(stderr, "image_transformer takes exactly 2 parameters.\n");
        return 0;
    }

    FILE *input = input_file(argv[1], "r");
    if (input == NULL) {
        fprintf(stderr, "Cannot open source file. \n");
        return -1;
    }

    FILE *output = input_file(argv[2], "w");
    if (output == NULL) {
        fprintf(stderr, "Cannot open target file. \n");
        return -1;
    }

    struct image image = {0};
    enum read_status read_status = from_bmp(input, &image);
    if (read_status == READ_OK) {
        struct image rotated = rotate(image);
        enum write_status write_status = to_bmp(output, &rotated);
        if (write_status == WRITE_OK) {
            fprintf(stdout, "Successfully performed operation \n");
            free(rotated.data);
            free(image.data);
            fclose(input);
            fclose(output);
        } else if (write_status == WRITE_ERROR) {
            fprintf(stderr, "Error when writing in file \n");
            free(rotated.data);
            free(image.data);
            fclose(input);
            fclose(output);
            return -1;
        }
        return 0;
    } else {
        if (read_status == READ_INVALID_HEADER) {
            fprintf(stderr, "Wrong image header passed \n");
        } else if (read_status == READ_INVALID_SIGNATURE) {
            fprintf(stderr, "Wrong signature passed \n");
        } else if (read_status == READ_INVALID_BITS) {
            fprintf(stderr, "Incorrect bits in file \n");
        }
        free(image.data);

        close_file(input);
        close_file(output);

        return -1;
    }

}
